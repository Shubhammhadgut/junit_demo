package demoApplication;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import junit.framework.TestResult;

public class CalculationTest extends TestCase {
	
	int value1=1;
	int value2=0;
	
	@Before
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		value1=10;
		value2=16;
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		value1=0;
		value2=0;
	}

	Calculation cal=new Calculation();
	
//	@Test
//	public void testAdd() {
//		int result=9;
//		assertEquals(result, cal.add(value1, value2));
//	}
//
//	@Test
//	public void testSub() {
//		int result=3;
//		assertEquals(result, cal.sub(value1, value2));
//	}
//
//	@Test
//	public void testMul() {
//		int result=18;
//		assertEquals(result, cal.mul(value1, value2));
//	}
//
//	@Test
//	public void testDiv() {
//		double result=0.5;
//		assertEquals(result, cal.div(value1, value2));
//	}
	
	//what is 3% of 6?
//	@Test
//	public void testPercent() {
//		double result=1.60;
//		assertEquals(result, cal.percent(value1, value2));
//	}

	//what percent of 60 is 12
	public void testPercentOf() {
		double result=20;
		assertEquals(result, cal.percentOfX(600, 125));
	}
	
	
}
