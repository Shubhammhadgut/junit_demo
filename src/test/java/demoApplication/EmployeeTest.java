package demoApplication;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {

	Object[] expecteEmp = new Object[3];
	
	@Before
	public void initInput() {
		expecteEmp[0]=new Employee(1, "shubham",1500);
		expecteEmp[1]=new Employee(2, "Omkar", 2000);
		expecteEmp[2]=new Employee(3, "Yash", 3800);
	}
	Employee emp=new Employee();
	@Test
	public void testEmp() {
		Employee emp1=new Employee(1, "shubham", 1500);
		assertEquals(emp1, emp.getEmp());
//		Object[] testOutput=emp.getEmp().toArray();
//		assertArrayEquals(expecteEmp,testOutput);
	}

}
