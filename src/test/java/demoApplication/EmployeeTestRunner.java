package demoApplication;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class EmployeeTestRunner {
	
	public static void main(String[] args) {
		Result rs=JUnitCore.runClasses(EmployeeTest.class);
		for(Failure fail: rs.getFailures()) {
			System.out.println(fail.getMessage());
		}
		System.out.println(rs.wasSuccessful());
	}

}
