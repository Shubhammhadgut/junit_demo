package demoApplication;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class CalculationTestRunner {

	public static void main(String[] args) {
		
		CalculationTest ct=new  CalculationTest();
		
		Result rs=JUnitCore.runClasses(CalculationTest.class);
		for(Failure fail:rs.getFailures()) {
			System.out.println(fail.getMessage());
		}
		System.out.println(rs.wasSuccessful());
		
		System.out.println(ct.value1);
		System.out.println(ct.value2);
		

	}

}
