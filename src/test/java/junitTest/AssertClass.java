package junitTest;

import static org.junit.Assert.*;

import org.junit.Test;

public class AssertClass {

	@Test
	public void test() {
		int num=5;
		String name="Shubham";
		String str=null;
		//check assertEqual
//		assertEquals("Shubham", name);
//		assertEquals("ram", name);
		
		//check assertFalse
//		assertFalse(num>3);//result is true //test failed
//		assertFalse(num<3);//true condition
		
		//check assertTrue
//		assertTrue(num<3);
//		assertTrue(num>3);//true condition
		
		//check assertNotNull
//		assertNotNull(str);//test failed
//		assertNotNull(name);//test true
		
		//check assertNull
//		assertNull(name);//test failed
//		assertNull(str);//test true
		
	}

	@Test
	public void myTestArray() {
		String[] expected= {"Shubham","Omkar","Yash","Mayur","Vaibhav"};
		
		String [] methodInput= {"Shubham","Omkar","Yash","Mayur","Vaibhav"};
		assertArrayEquals(expected, methodInput);
		
	}
	

}
