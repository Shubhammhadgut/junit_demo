package junitTest;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class MessageTestRunner {
	
	public static void main(String [] args) {
		Result rs=JUnitCore.runClasses(MessageTest.class);
		for(Failure fail:rs.getFailures()) {
			System.out.println(fail.getMessage());
		}
		System.out.println(rs.wasSuccessful());
	}

}
