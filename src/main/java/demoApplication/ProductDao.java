package demoApplication;

public class ProductDao {
	Product p=new Product();
	double discount=0;
	int dprice=0;
	public int discountOnProduct(Product p) {
		int price=p.getPrice();
		if(price>1000) {
			discount=0.2;
			dprice=(int) (price-price*discount);
		}
		else {
			discount=0.1;
			dprice=(int) (price-(price*discount));
		}
		return dprice;
	}
	
}
