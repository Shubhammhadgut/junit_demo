package demoApplication;

public class Calculation {
	
	public int add(int a,int b) {
		return a+b;
	}
	
	public int sub(int a,int b) {
		if(a>b) {
			return a-b;
		}
		else {
			return b-a;
		}
	}
	
	public int mul(int a,int b) {
		return a*b;
	}
	
	public double div(double a,double b) {
		if(b==0) {
			return 0;
		}
		return a/b;
	}
	//What is a% of b
	public double percent(double a,double b) {
		b=(a/100)*b;
		return b;
	}
	
	//what percent of X is Y
	public double percentOfX(double x,double y) {
		double z;
		z=(y/x)*100;
		return z;
	}

}
