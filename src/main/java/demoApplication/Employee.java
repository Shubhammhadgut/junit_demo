package demoApplication;

import java.util.ArrayList;
import java.util.List;

public class Employee {
	
	int id;
	String name;
	int salary;
	 
	Employee(int id,String name,int salary){
		this.id=id;
		this.name=name;
		this.salary=salary;
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public Employee getEmp() {
		return new Employee(1, "shubham", 1500);
	}

//	public List<Employee> getEmp(){
//		
//		List<Employee> li=new ArrayList<Employee>();
//		li.add(new Employee(1, "shubham", 1500));
//		li.add(new Employee(2, "Omkar", 2000));
//		li.add(new Employee(3, "Yash", 3800));
//		return li;
//		
//	}
}
